<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'namespace' => 'Api'], function(){
	// Auth
    Route::post('/user/login', 'AuthController@login');
    Route::post('/user/login/token', 'AuthController@loginToken');
    Route::post('/user/register', 'AuthController@register');

    // Friend
    Route::post('/user/friend/request', 'FriendController@requestFriend');
    Route::post('/user/friend/accept', 'FriendController@acceptFriend');
    Route::post('/user/friend/reject', 'FriendController@rejectFriend');
    Route::post('/user/friend/remove', 'FriendController@removeFriend');
    Route::post('/user/friends/list', 'FriendController@listFriends');

    // Block
    Route::post('/user/block', 'FriendController@block');
    Route::post('/user/unblock', 'FriendController@unblock');
    Route::post('/user/enemies/list', 'FriendController@listEnemies');

    // Message
    Route::post('/user/message/send', 'MessageController@sendMessage');
    Route::post('/user/messages/receive', 'MessageController@receiveMessages');
    Route::post('/user/messages/list', 'MessageController@listMessages');

    //User
    Route::post('/user/profile', 'UserController@getProfile');
});
