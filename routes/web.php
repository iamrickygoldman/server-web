<?php

use App\Models\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Controller@showHome')->name('home');

// Access group SuperAdmin
Route::middleware(['check_role:'.Role::SUPERADMIN_ID], function(){})->group(function(){

	// Roles
	Route::get('/admin/roles', 'RoleController@showListRole')->name('role-list');
	Route::get('/admin/role/new', 'RoleController@showNewRole')->name('role-new');
	Route::get('/admin/role/edit/{id}', 'RoleController@showEditRole')->name('role-edit');
	Route::post('/admin/role/save', 'RoleController@saveRole')->name('role-save');
	Route::post('/admin/role/save-and-close', 'RoleController@saveAndCloseRole')->name('role-save-and-close');
	Route::post('/admin/role/delete', 'RoleController@deleteRole')->name('role-delete');

	// Users
	Route::get('/admin/users', 'UserController@showListUser')->name('user-list');
	Route::get('/admin/user/edit/{id}', 'UserController@showEditUser')->name('user-edit');
	Route::post('/admin/user/save', 'UserController@saveUser')->name('user-save');
	Route::post('/admin/user/save-and-close', 'UserController@saveAndCloseUser')->name('user-save-and-close');
	Route::post('/admin/user/deactivate/{id}', 'UserController@deactivateUser')->name('user-deactivate');
	Route::post('/admin/user/activate/{id}', 'UserController@activateUser')->name('user-activate');
	Route::post('/admin/user/delete/{id}', 'UserController@deleteUser')->name('user-delete');

	// Server Data
	Route::get('/admin/server/activity-monitor', 'ServerController@showActivityMonitor')->name('server-activity-monitor');
	Route::post('/admin/server/activity-monitor-update', 'ServerController@updateActivityMonitor')->name('server-activity-monitor-update');
	Route::post('/admin/server/shutdown', 'ServerController@shutdown')->name('server-shutdown');
	Route::post('/admin/server/disconnect', 'ServerController@disconnect')->name('server-disconnect');
	Route::get('/admin/server/ip-block', 'ServerController@showIpBlock')->name('server-ip-block');
	Route::post('/admin/server/ip-block/save', 'ServerController@saveIpBlock')->name('server-ip-block-save');

});

// Access groups SuperAdmin and Admin
Route::middleware(['check_role:'.Role::SUPERADMIN_ID.','.Role::ADMIN_ID], function(){})->group(function(){

	// Dashboard
	Route::get('/admin', 'Controller@showDashboard')->name('dashboard');

});