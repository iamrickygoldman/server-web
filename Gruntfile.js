var grunt = require('grunt');

module.exports = function(grunt){
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask('default', ['scss', 'uglify', 'watch']);
	
	grunt.initConfig({
		concat: {
			dist: {
				src: [
					'node_modules/sumoselect/sumoselect.min.css',
					'public/css/admin.css'
				],
				dest: 'public/css/admin-combined.min.css'
			}
		},
		sass: {
		    dist: {
		    	options: {
		        	style: 'compressed'
		    	},
			  	files: [
				  	{
						'public/css/admin.css': 'resources/scss/admin.scss'
			    	},
		    	]
			}
		},
		uglify: {
			options: {
				mangle: false
			},
			target: {
				files: {
					'public/js/admin-combined.min.js':
					[
						'node_modules/jquery/dist/jquery.min.js',
						'node_modules/sumoselect/jquery.sumoselect.min.js',
						'resources/js/jquery.sumoselect.min.js',
						'resources/js/admin.js'
					]
				}
			}
		},
		watch: {
			css: {
				files: [
					'resources/scss/*.scss',
				],
				tasks: ['sass','concat'],
				options: {
					livereload: true
				},
			},
			js: {
				files: 'resources/js/admin.js',
				tasks: ['uglify'],
				options: {
					livereload: true
				},
			},
		}
	});
};