<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\Role;

class RoleController extends Controller
{
    private $baseMenuRoute;

    public function __contruct()
    {
    	$this->baseMenuRoute = '/roles';
    }

    public function showListRole()
    {
    	$params = array(
    		'mainMenu' => Helper::getAdminMainMenu($this->baseMenuRoute),
    		'roles' => Role::all(),
    	);

    	return view('admin.pages.role.list', $params);
    }

    public function showEditRole($id)
    {
        $role = Role::find($id);
        if (in_array($role->id, array(1,2,3)))
        {
            return redirect()->route('role-list');
        }

    	$params = array(
    		'mainMenu' => Helper::getAdminMainMenu($this->baseMenuRoute),
    		'role' => $role,
    	);

    	return view('admin.pages.role.edit', $params);
    }

    public function showNewRole()
    {
    	$params = array(
    		'mainMenu' => Helper::getAdminMainMenu($this->baseMenuRoute),
    	);

    	return view('admin.pages.role.new', $params);
    }

    public function saveRole(Request $request)
    {
    	$role = $this->save($request);

    	return redirect()->route('role-edit', ['id' => $role->id]);
    }

    public function saveAndCloseRole(Request $request)
    {
    	$this->save($request);

    	return redirect()->route('role-list');
    }

    private function save(Request $request)
    {
    	$validatedData = $request->validate([
    		'id' => 'nullable|integer',
    		'title' => 'required|string|max:255',
    		'description' => 'required|string',
    	]);

    	if ($request->id) {
    		$role = Role::find($request->id);
            if (in_array($role->id, array(1,2,3)))
            {
                return redirect()->route('role-list');
            }
    	}
    	else {
    		$role = new Role;
    	}

    	$role->title = $request->title;
    	$role->description = $request->description;

    	$role->save();

    	$request->session()->flash('status-success', 'Role '.$role->title.' successfully saved!');

    	return $role;
    }

    public function deleteRole(Request $request, $id)
    {
    	$role = Role::find($id);
        if (in_array($role->id, array(1,2,3)))
        {
            return redirect()->route('role-list');
        }
    	$role->delete();

    	return redirect()->route('role-list');
    }
}
