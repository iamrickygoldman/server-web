<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Helpers\Helper;
use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
	{
	    $rules = [
	        'username' => 'unique:users|required|alpha_num|max:30|min:2',
	        'email'    => 'unique:users|required|email',
	        'password' => 'required|min:6',
	        'password_confirmation' => 'required|same:password',
	    ];

	    $input = $request->only('username', 'email', 'password', 'password_confirmation');
	    $validator = Validator::make($input, $rules);
	    if ($validator->fails()) {
	        return response()->json([
	        	'success' => false,
	        	'error' => 150,
	        	'message' => $validator->errors()->first(),
	        ]);
	    }

	    $user = new User();
	    $user->username = $request->username;
	    $user->email = $request->email;
	    $user->password = Hash::make($request->password);
	    $result = $user->save();

	    $token = $user->generateApiToken();
	    if (!$result)
	    {
	    	return response()->json([
	        	'success' => false,
	        	'error' => 151,
	        	'message' => Helper::getError(151),
	        ]);
	    }
	    else
	    {
		    return response()->json([
		    	'success' => true,
		    	'id' => $user->id,
		    	'token' => $token,
		    	'username' => $user->username,
		    	'mmr' => 1200,
		    ]);
		}
	}

	public function login(Request $request)
	{
		$validatedData = $request->validate([
    		'username' => 'required|alpha_num',
    		'password' => 'required|string',
    	]);

		$user = User::get()->where('username', '=', $request->username)->last();
		if (is_null($user))
		{
			return response()->json([
				'success' => false,
				'error' => 101,
				'message' => Helper::getError(101),
			]);
		}
		if (Hash::check($request->password, $user->password))
		{
			$token = $user->generateApiToken();
			return response()->json([
				'success' => true,
				'id' => $user->id,
				'token' => $token,
				'username' => $user->username,
				'mmr' => $user->mmr,
 			]);
		}
		else
		{
			return response()->json([
				'success' => false,
				'error' => 110,
				'message' => Helper::getError(110),
			]);
		}
	}

	public function loginToken(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60'
		]);

		$user = User::find($request->id);
		if (is_null($user))
		{
			return response()->json([
				'success' => false,
				'error' => 100,
				'message' => Helper::getError(100),
			]);
		}
		$result = $user->validateToken($request->token);
		switch ($result)
		{
			case 112:
			case 111:
				return response()->json([
					'success' => false,
					'error' => $result,
					'message' => Helper::getError($result),
				]);
			default:
				return response()->json([
					'success' => true,
					'username' => $user->username,
					'mmr' => $user->mmr,
				]);
		}
	}
}
