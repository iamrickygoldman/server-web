<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;
use App\Models\User;
use App\Models\Message;

class MessageController extends Controller
{
    public function sendMessage(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'receiver_id' => 'required|string',
			'message' => 'required|string',
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$receiver = User::where('id', '=', $request->receiver_id)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $receiver);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			$result = Message::sendMessage($user->id, $receiver->id, $request->message);
			switch ($result)
			{
				case 310:
					return response()->json([
						'success' => false,
						'error' => $result,
						'message' => Helper::getError($result),
					]);
				default:
					return response()->json([
						'success' => true,
						'receiver_id' => $receiver->id,
					]);
			}
		}
	}

	public function receiveMessages(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'sender_id' => 'required|string',
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$sender = User::where('id', '=', $request->sender_id)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $sender);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			Message::receiveMessages($sender->id, $user->id);
			return response()->json([
				'success' => true,
				'sender_id' => $sender->id,
			]);
		}
	}

	public function listMessages(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'other_id' => 'required|string',
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$other = User::where('id', '=', $request->other_id)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $other);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			$messages = array();
			foreach ($user->messages($other->id) as $message)
			{
				$tmp = new \stdClass;
				$tmp->message = $message->message;
				$tmp->time_sent = $message->time_sent;
				$tmp->received = $message->received;
				$tmp->sender_id = $message->sender_id;
				$messages[] = $tmp;
			}

			return response()->json([
				'success' => true,
				'messages' => $messages,
			]);
		}
	}

	private function validateUsers($sender, $token, $receiver)
	{
		$return = array();
		if (is_null($sender))
		{
			$return['success'] = false;
			$return['error'] = 100;
			$return['message'] = Helper::getError(100);
			return $return;
		}
		$result = $sender->validateToken($token);
		switch ($result)
		{
			case 112:
			case 111:
				$return['success'] = false;
				$return['error'] = $result;
				$return['message'] = Helper::getError($result);
				return $return;
		}
		if (is_null($receiver))
		{
			$return['success'] = false;
			$return['error'] = 300;
			$return['message'] = Helper::getError(300);
			return $return;
		}

		$return['success'] = true;
		return $return;
	}
}
