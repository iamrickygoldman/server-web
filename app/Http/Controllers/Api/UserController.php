<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\User;

class UserController extends Controller
{
    public function getProfile(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'user_id' => 'required|integer'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$profile = User::where('id', '=', $request->user_id)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $profile);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			$data = new \stdClass;
			$data->id = $profile->id;
			$data->username = $profile->username;
			$data->mmr = $profile->mmr;
			$data->is_pending = $profile->isPendingFriend($user->id);
			$data->can_accept_friend = $user->canAcceptFriend($profile->id);
			$data->is_friend = $profile->isFriend($user->id);
			$data->is_blocked = $profile->isBlocked($user->id);
			
			return response()->json([
				'success' => true,
				'profile' => $data,
			]);
		}
	}

	private function validateUsers($user, $token, $other)
	{
		$return = array();
		if (is_null($user))
		{
			$return['success'] = false;
			$return['error'] = 100;
			$return['message'] = Helper::getError(100);
			return $return;
		}
		$result = $user->validateToken($token);
		switch ($result)
		{
			case 112:
			case 111:
				$return['success'] = false;
				$return['error'] = $result;
				$return['message'] = Helper::getError($result);
				return $return;
		}
		if (is_null($other))
		{
			$return['success'] = false;
			$return['error'] = 400;
			$return['message'] = Helper::getError(400);
			return $return;
		}

		$return['success'] = true;
		return $return;
	}
}
