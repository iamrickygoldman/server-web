<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Helper;
use App\Models\User;

class FriendController extends Controller
{
    public function requestFriend(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$friend = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $friend);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			$result = $user->requestFriend($friend->id);
			switch ($result)
			{
				case 210:
				case 211:
				case 214:
				case 215:
					return response()->json([
						'success' => false,
						'error' => $result,
						'message' => Helper::getError($result),
					]);
				default:
					return response()->json([
						'success' => true,
						'friend_id' => $friend->id,
					]);
			}
		}
	}

	public function acceptFriend(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$friend = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $friend);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			if ($user->acceptFriend($friend->id))
			{
				return response()->json([
					'success' => true,
					'friend_id' => $friend->id,
				]);
			}
			else
			{
				return response()->json([
					'success' => false,
					'error' => 212,
					'message' => Helper::getError(212),
				]);
			}
		}
	}

	public function rejectFriend(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$friend = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $friend);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			if ($user->rejectFriend($friend->id))
			{
				return response()->json([
					'success' => true,
					'friend_id' => $friend->id,
				]);
			}
			else
			{
				return response()->json([
					'success' => false,
					'error' => 212,
					'message' => Helper::getError(212),
				]);
			}
		}
	}

	public function removeFriend(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$friend = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $friend);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			if ($user->removeFriend($friend->id))
			{
				return response()->json([
					'success' => true,
					'friend_id' => $friend->id,
				]);
			}
			else
			{
				return response()->json([
					'success' => false,
					'error' => 213,
					'message' => Helper::getError(213),
				]);
			}
		}
	}

	public function listFriends(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		if (is_null($user))
		{
			return response()->json([
				'success' => false,
				'error' => 100,
				'message' => Helper::getError(100),
			]);
		}
		$result = $user->validateToken($request->token);
		switch ($result)
		{
			case 112:
			case 111:
				return response()->json([
					'success' => false,
					'error' => $result,
					'message' => Helper::getError($result),
				]);
			default:
				$friends = array();
				$pending = array();
				foreach ($user->friends as $friend)
				{
					$tmp = new \stdClass;
					$tmp->id = $friend->id;
					$tmp->username = $friend->username;
					$tmp->mmr = $friend->mmr;
					if ($friend->pivot->confirmed)
					{
						$friends[] = $tmp;
					}
					else
					{
						$pending[] = $tmp;
					}
				}
				return response()->json([
					'success' => true,
					'friends' => $friends,
					'pending' => $pending,
				]);
		}
	}

	public function block(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$enemy = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $enemy);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			if ($user->block($enemy->id))
			{
				return response()->json([
					'success' => true,
					'enemy_id' => $enemy->id,
				]);
			}
			else
			{
				return response()->json([
					'success' => false,
					'error' => 252,
					'message' => Helper::getError(252),
				]);
			}
		}
	}

	public function unblock(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
			'username' => 'required|string'
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		$enemy = User::where('username', '=', $request->username)->where('role_id', '=', 3)->first();
		$auth = $this->validateUsers($user, $request->token, $enemy);
		if (!$auth['success'])
		{
			return response()->json([
				'success' => false,
				'error' => $auth['error'],
				'message' => $auth['message'],
			]);
		}
		else
		{
			if ($user->unblock($enemy->id))
			{
				return response()->json([
					'success' => true,
					'enemy_id' => $enemy->id,
				]);
			}
			else
			{
				return response()->json([
					'success' => false,
					'error' => 253,
					'message' => Helper::getError(253),
				]);
			}
		}
	}

	public function listEnemies(Request $request)
	{
		$validatedData = $request->validate([
			'id' => 'required|integer',
			'token' => 'required|string|size:60',
		]);

		$user = User::where('id', '=', $request->id)->where('role_id', '=', 3)->first();
		if (is_null($user))
		{
			return response()->json([
				'success' => false,
				'error' => 100,
				'message' => Helper::getError(100),
			]);
		}
		$result = $user->validateToken($request->token);
		switch ($result)
		{
			case 112:
			case 111:
				return response()->json([
					'success' => false,
					'error' => $result,
					'message' => Helper::getError($result),
				]);
			default:
				$enemies = array();
				foreach ($user->enemies as $enemy)
				{
					$tmp = new \stdClass;
					$tmp->id = $enemy->id;
					$tmp->username = $enemy->username;
					$enemies[] = $tmp;
				}
				return response()->json([
					'success' => true,
					'enemies' => $enemies,
				]);
		}
	}

	private function validateUsers($user, $token, $friend)
	{
		$return = array();
		if (is_null($user))
		{
			$return['success'] = false;
			$return['error'] = 100;
			$return['message'] = Helper::getError(100);
			return $return;
		}
		$result = $user->validateToken($token);
		switch ($result)
		{
			case 112:
			case 111:
				$return['success'] = false;
				$return['error'] = $result;
				$return['message'] = Helper::getError($result);
				return $return;
		}
		if (is_null($friend))
		{
			$return['success'] = false;
			$return['error'] = 200;
			$return['message'] = Helper::getError(200);
			return $return;
		}

		$return['success'] = true;
		return $return;
	}
}
