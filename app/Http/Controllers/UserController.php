<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    private $baseMenuRoute;

    public function __construct()
    {
    	$this->baseMenuRoute = '/users';
    }

    public function showListUser(Request $request)
    {
        $validatedData = $request->validate([
            'filters[username]' => 'nullable|string|max:255',
            'filters[email]' => 'nullable|string|email|max:255',
            'filters[role_id]' => 'nullable|integer',
            'filters[limit]' => 'nullable|integer',
            'filters[sort]' => 'nullable|string|max:255',
            'filters[direction]' => 'nullable|string|max:4',
        ]);

        $limit = is_null($request->filters['limit']) ? 25 : $request->filters['limit'];
        $sort = is_null($request->filters['sort']) ? 'id' : $request->filters['sort'];
        $direction = is_null($request->filters['direction']) ? 'asc' : $request->filters['direction'];

        $filter_values = array(
            'username' => $request->filters['username'],
            'email' => $request->filters['email'],
            'role_id' => $request->filters['role_id'],
            'limit' => $limit,
            'sort' => $sort,
            'direction' => $direction,
        );

        $q = User::withTrashed();
        $q->orderBy($filter_values['sort'], $filter_values['direction']);

        if (!is_null($filter_values['username']))
        {
            $q->where('username','LIKE','%'.$filter_values['username'].'%');
        }
        if (!is_null($filter_values['email']))
        {
            $q->where('email','LIKE','%'.$filter_values['email'].'%');
        }
        if (!is_null($filter_values['role_id']))
        {
            $q->where('role_id','=',$filter_values['role_id']);
        }

        if (strtolower($limit) === 'all')
        {
            $users = $q->get();
        }
        else
        {
            $users = $q->paginate($filter_values['limit']);
        }

    	$params = array(
    		'mainMenu' => Helper::getAdminMainMenu($this->baseMenuRoute),
    		'users' => $users,
            'roles' => Role::all(),
            'filter_values' => $filter_values,
            'pagination_options' => Helper::getPaginationOptions(),
    	);

    	return view('admin.pages.user.list', $params);
    }

    public function showEditUser($id)
    {
    	$params = array(
    		'mainMenu' => Helper::getAdminMainMenu($this->baseMenuRoute),
    		'user' => User::withTrashed()->where('id',$id)->first(),
    		'roles' => Role::all(),
    		'backLink' => route('user-list'),
    	);

    	return view('admin.pages.user.edit', $params);
    }

    public function saveUser(Request $request)
    {
    	$user = $this->save($request);

    	return redirect()->route('user-edit', ['id' => $user->id]);
    }

    public function saveAndCloseUser(Request $request)
    {
    	$this->save($request);

    	return redirect()->route('user-list');
    }

    private function save(Request $request)
    {
        $unique_rule = $request->id ? ','.$request->id.',id' : '';
    	$validatedData = $request->validate([
    		'id' => 'required|integer',
    		'username' => 'required|string|max:255|unique:users,username'.$unique_rule,
    		'email' => 'required|string|email|max:255|unique:users,email'.$unique_rule,
    		'password' => 'nullable|string|min:6|confirmed',
    		'role_id' => 'nullable|integer',
    	]);

    	$user = User::withTrashed()->where('id', $request->id)->first();
    	$user->username = $request->username;
    	$user->email = $request->email;

    	if ($request->password)
    	{
    		$user->password = Hash::make($request->password);
    	}

    	if ($request->role_id == '')
    	{
    		$user->role_id = null;
    	}
    	else
    	{
    		$user->role_id = (int)$request->role_id;
    	}

    	$user->save();

    	$request->session()->flash('status-success', 'User '.$user->username.' successfully saved!');

    	return $user;
    }

    public function deleteUser(Request $request, $id)
    {
    	$user = User::withTrashed()->where('id', $id)->first();
    	$user->forceDelete();

    	return redirect()->route('user-list');
    }

    public function deactivateUser(Request $request, $id)
    {
    	$user = User::withTrashed()->where('id', $id)->first();
    	$user->delete();

    	return redirect()->route('user-list');
    }

    public function activateUser(Request $request, $id)
    {
    	$user = User::withTrashed()->where('id', $id)->first();
    	$user->restore();

    	return redirect()->route('user-list');
    }
}
