<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\ServerConnection;
use App\Models\IpBlock;

class ServerController extends Controller
{
    public function showActivityMonitor()
    {
    	$mainMenu = Helper::getAdminMainMenu('/admin/server/activity-monitor');

    	$current_connections = ServerConnection::whereNull('time_disconnected')->orderBy('id', 'DESC')->get();

    	$params = array(
	    	'mainMenu' => $mainMenu,
	    	'current_connections' => $current_connections,
	    );

	    return view('admin.pages.server.activity-monitor', $params);
    }

    public function updateActivityMonitor()
    {
    	$current_connections = ServerConnection::whereNull('time_disconnected')->orderBy('id', 'DESC')->get();

    	return response()->json([
    		'success' => true,
	    	'current_connections' => $current_connections,
	    ]);

	    return view('admin.pages.server.activity-monitor', $params);
    }

    public function shutdown()
    {
    	$message = '#x' . $this->Pack(config('gameserver.shutdown_password'), 'X');
    	return $this->Send($message, true);
    }

    public function disconnect(Request $request)
    {
    	$validatedData = $request->validate([
            'ip' => 'required|ip',
            'port' => 'required|integer|max:65535|min:1',
        ]);

    	$message = '#x' . $this->pack(config('gameserver.shutdown_password'), 'X') . $this->pack($request->ip, 'I') . $this->pack($request->port, 'P');
    	return $this->send($message, true);
    }

    private function send($message, $ajax = false)
    {
    	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    	if (!$socket)
    	{
    		return response()->json([
    			'success' => false,
    			'error' => null,
    			'message' => 'Failed to create socket.',
    		]);
    	}
    	if (socket_bind($socket, config('gameserver.address')))
    	{
    		try {
	    		if (socket_connect($socket, config('gameserver.address'), (int)config('gameserver.port')))
	    		{
	    			$message .= '`';
	    			if (socket_send($socket, $message, strlen($message), MSG_EOF))
	    			{
	    				if ($ajax)
	    				{
		    				return response()->json([
		    					'success' => true
		    				]);
		    			}
		    			else
		    			{
		    				return true;
		    			}
	    			}
	    			else
	    			{
	    				$success = false;
	    			}
	    		}
	    		else
		    	{
		    		$success = false;
		    	}
		    } catch (\ErrorException $e) {
		    	$success = false;
		    }
    	}
    	else
    	{
    		$success = false;
    	}
    	if (!$success)
    	{
    		if ($ajax)
    		{
	    		$error = socket_last_error($socket);
	    		return response()->json([
	    			'success' => false,
	    			'error' => $error,
	    			'message' => socket_strerror($error),
	    		]);
	    	}
	    	else
	    	{
	    		return false;
	    	}
    	}
    }

    private function pack($data, $marker)
    {
    	return '{' . $marker . $data . $marker . '}';
    }

    public function showIpBlock()
    {
    	$mainMenu = Helper::getAdminMainMenu('/admin/server/activity-monitor');

    	$ip_blocks = IpBlock::get();
    	$ipv4 = array();
    	$ipv6 = array();
    	foreach ($ip_blocks as $item)
    	{
    		switch ($item->type)
    		{
    			case 'ipv4':
    				$ipv4 = $item->list;
    				break;
    			case 'ipv6':
    				$ipv6 = $item->list;
    				break;
    		}
    	}

    	$params = array(
	    	'mainMenu' => $mainMenu,
	    	'ipv4' => $ipv4,
	    	'ipv6' => $ipv6,
	    );

	    return view('admin.pages.server.ip-block', $params);
    }

    public function saveIpBlock(Request $request)
    {
    	$validatedData = $request->validate([
    		'ipv4' => 'nullable|string',
    		'ipv6' => 'nullable|string',
    	]);

    	$ipv4 = IpBlock::where('type','LIKE','ipv4')->first();
    	if (is_null($ipv4))
    	{
    		$ipv4 = new IpBlock;
    		$ipv4->type = 'ipv4';
    	}
    	$ipv6 = IpBlock::where('type','LIKE','ipv6')->first();
    	if (is_null($ipv6))
    	{
    		$ipv6 = new IpBlock;
    		$ipv6->type = 'ipv6';
    	}

    	$ipv4->list = array_map('trim', explode(PHP_EOL, $request->ipv4));
		$ipv4->save();

		$ipv6->list = array_map('trim', explode(PHP_EOL, $request->ipv6));
		$ipv6->save();

		$message = '?X' . $this->Pack(config('gameserver.shutdown_password'), 'X');
		if ($this->send($message))
		{
			$request->session()->flash('status-success', 'Successfully saved!');
		}
		else
		{
			$request->session()->flash('status-warning', 'Successfully saved, but the game server was not notified.');
		}

    	return redirect()->route('server-ip-block');
    }
}
