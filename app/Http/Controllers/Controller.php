<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

use App\Helpers\Helper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function showHome()
    {
        return view('site.pages.home');
    }

    public function showDashboard()
    {
    	$mainMenu = Helper::getAdminMainMenu('/');
    	$tiles = array();
    	foreach ($mainMenu as $item)
    	{
    		$include = false;
    		$tile = '<div class="tile col-sm-6 col-md-4">';
    		$tile .= '<a href="'.$item->url.'">';
    		switch ($item->url)
    		{
    			case '/':
    				break;
    			case '/users':
    				$include = true;
    				$tile .= '<h2>Users</h2>';
    				$tile .= '<img src="/images/tile-users.png" alt="">';
    				break;
    			case '/roles':
    				$include = true;
    				$tile .= '<h2>Roles</h2>';
    				$tile .= '<img src="/images/tile-roles.png" alt="">';
    				break;
    		}
    	}
    	$tile .= '</a>';
    	$tile .= '</div>';
    	if ($include)
    	{
    		$tiles[] = $tile;
    	}

    	$params = array(
	    	'mainMenu' => $mainMenu,
	    	'tiles' => $tiles,
	    );

	    return view('admin.pages.dashboard', $params);
    }
}
