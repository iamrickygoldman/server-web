<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Support\Facades\Gate;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$level)
    {
        $denied = true;
        foreach ($level as $role)
        {
            if (Gate::allows('role', $role))
            {
                $denied = false;
            }
        }
        if ($denied)
        {
            $request->session()->flash('status-warning', 'You do not have permission to view the requested page and were redirected.');
            return redirect()->route('home');
        }

        return $next($request);
    }
}
