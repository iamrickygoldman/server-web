<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const SUPERADMIN_TITLE = 'SuperAdmin';
    const SUPERADMIN_ID = 1;
    const ADMIN_TITLE = 'Admin';
    const ADMIN_ID = 2;
    const USER_TITLE = 'User';
    const USER_ID = 3;

    public $timestamps = false;

    public function users()
    {
    	return $this->hasMany('App\Models\User');
    }
}
