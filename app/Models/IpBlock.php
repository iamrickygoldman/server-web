<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IpBlock extends Model
{
	public $timestamps = false;
	
    public function getListAttribute($value)
    {
    	$return = json_decode($value, true);
    	if (!is_array($return))
    	{
    		return array();
    	}
    	return $return;
    }

    public function setListAttribute($value)
    {
    	$this->attributes['list'] = json_encode($value);
    }
}
