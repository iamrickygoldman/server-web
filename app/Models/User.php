<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Role;
use App\Models\Message;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function serverConnections()
    {
        return $this->hasMany('App\Models\ServerConnection');
    }

    public function isAdmin()
    {
        $role_id = $this->role->id;
        return $role_id == Role::SUPERADMIN_ID || Role::ADMIN_ID;
    }

    public function generateApiToken()
    {
        $token = Str::random(60);
        $this->updateApiToken($token);

        return $token;
    }

    public function updateApiToken($token)
    {
        $this->forceFill([
            'api_token' => $token,
            'api_token_refreshed' => Carbon::now()->format('Y-m-d H:i:s'),
        ])->save();
    }

    public function validateToken($token)
    {
        if ($this->api_token === $token)
        {
            $expires = Carbon::now();
            if (!is_null($this->api_token_refreshed))
            {
                $expires = new Carbon($this->api_token_refreshed);
            }
            if ($expires->addWeek()->greaterThanOrEqualTo(Carbon::now()))
            {
                return -1;
            }
            else
            {
                return 112;
            }
        }
        else
        {
            return 111;
        }
    }

    public function requestedFriends()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'requester_id', 'receiver_id')->withPivot('confirmed')->wherePivot('confirmed', '=', true);
    }

    public function receivedFriends()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'receiver_id', 'requester_id')->withPivot('confirmed')->wherePivot('confirmed', '=', true);
    }

    public function pendingFriends()
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'receiver_id', 'requester_id')->withPivot('confirmed')->wherePivot('confirmed', '=', false);
    }

    public function getFriendsAttribute()
    {
        if (!array_key_exists('friends', $this->relations))
        {
            $this->loadFriends();

            return $this->getRelation('friends');
        }
    }

    private function loadFriends()
    {
        if (!array_key_exists('friends', $this->relations))
        {
            $friends = $this->mergeFriends();

            $this->setRelation('friends', $friends);
        }
    }

    private function mergeFriends()
    {
        return $this->pendingFriends->merge($this->requestedFriends->merge($this->receivedFriends));
    }

    public function requestFriend($friend_id)
    {
        if ($this->isFriend($friend_id))
        {
            return 210;
        }
        else if ($this->isPendingFriend($friend_id))
        {
            return 211;
        }
        else if ($this->canUnblock($friend_id))
        {
            return 214;
        }
        else if ($this->isBlocked($friend_id))
        {
            return 215;
        }
        else
        {
            $this->requestedFriends()->attach($friend_id);
            return -1;
        }
    }

    public function acceptFriend($friend_id)
    {
        if ($this->canAcceptFriend($friend_id))
        {
            DB::table('friends')
                ->where('receiver_id', '=', $this->id)
                ->where('requester_id', '=', $friend_id)
                ->where('confirmed', '=', false)
                ->update(['confirmed' => true]);

            return true;
        }
        else
        {
            return false;
        }
    }

    public function rejectFriend($friend_id)
    {
        if ($this->canAcceptFriend($friend_id))
        {
            DB::table('friends')
                ->where('receiver_id', '=', $this->id)
                ->where('requester_id', '=', $friend_id)
                ->delete();

            return true;
        }
        else
        {
            return false;
        }
    }

    public function removeFriend($friend_id)
    {
        if ($this->canRemoveFriend($friend_id))
        {
            DB::table('friends')
                ->where(function($q) use($friend_id) {
                    $q->where('requester_id', '=', $this->id)
                      ->where('receiver_id', '=', $friend_id);
                })
                ->orWhere(function($q) use($friend_id) {
                    $q->where('receiver_id', '=', $this->id)
                      ->where('requester_id', '=', $friend_id);
                })
                ->delete();

            return true;
        }
        else
        {
            return false;
        }
    }

    public function isFriend($friend_id)
    {
        $requested = $this->requestedFriends->contains($friend_id);
        $received = $this->receivedFriends->contains($friend_id);

        return $requested || $received;
    }

    public function isPendingFriend($friend_id)
    {
        $pending = DB::table('friends')
            ->where(function($q) use($friend_id) {
                $q->where(function($w) use($friend_id) {
                    $w->where('requester_id', '=', $this->id)
                      ->where('receiver_id', '=', $friend_id);
                })
                  ->orWhere(function($w) use($friend_id) {
                    $w->where('receiver_id', '=', $this->id)
                      ->where('requester_id', '=', $friend_id);
                });
            })
            ->where('confirmed', '=', false)
            ->count();

        return $pending === 1;
    }

    public function canAcceptFriend($friend_id)
    {
        $pending = DB::table('friends')
            ->where('receiver_id', '=', $this->id)
            ->where('requester_id', '=', $friend_id)
            ->where('confirmed', '=', false)
            ->count();

        return $pending === 1;
    }

    private function canRemoveFriend($friend_id)
    {
        $exists = DB::table('friends')
            ->where(function($q) use($friend_id) {
                $q->where('requester_id', '=', $this->id)
                  ->where('receiver_id', '=', $friend_id);
            })
            ->orWhere(function($q) use($friend_id) {
                $q->where('receiver_id', '=', $this->id)
                  ->where('requester_id', '=', $friend_id);
            })
            ->count();

        return $exists === 1;
    }

    public function enemies()
    {
        return $this->belongsToMany('App\Models\User', 'enemies', 'requester_id', 'blocked_id');
    }

    public function block($enemy_id)
    {
        if ($this->canBlock($enemy_id))
        {
            $this->enemies()->attach($enemy_id);
            return true;
        }
        else
        {
            return false;
        }
    }

    public function unblock($enemy_id)
    {
        if ($this->canUnblock($enemy_id))
        {
            $this->enemies()->detach($enemy_id);
            return true;
        }
        else
        {
            return false;
        }
    }

    private function canBlock($enemy_id)
    {
        $exists = DB::table('enemies')
            ->where('requester_id', '=', $this->id)
            ->where('blocked_id', '=', $enemy_id)
            ->count();

        return $exists < 1;
    }

    private function canUnblock($enemy_id)
    {
        return !$this->canBlock($enemy_id);
    }

    public function isBlocked($enemy_id)
    {
        $exists = DB::table('enemies')
            ->where('blocked_id', '=', $this->id)
            ->where('requester_id', '=', $enemy_id)
            ->count();

        return $exists > 0;
    }

    public function messages($other_id)
    {
        return Message::where(function($q) use($other_id) {
                $q->where('sender_id', '=', $this->id)
                  ->where('receiver_id', '=', $other_id);
            })
            ->orWhere(function($q) use($other_id) {
                $q->where('sender_id', '=', $other_id)
                  ->where('receiver_id', '=', $this->id);
            })
            ->orderBy('time_sent')->get();
    }
}
