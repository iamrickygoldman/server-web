<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Message extends Model
{
	public $timestamps = false;

    public function sender()
    {
    	return $this->belongsTo('App\Models\User', 'messages', 'sender_id');
    }

    public function receiver()
    {
    	return $this->belongsTo('App\Model\User', 'messages', 'receiver_id');
    }

    public static function sendMessage($sender_id, $receiver_id, $message_text)
    {
        if (strlen($message_text) === 0)
        {
            return 310;
        }
        $message = new Message;
        $message->sender_id = $sender_id;
        $message->receiver_id = $receiver_id;
        $message->message = $message_text;
        $message->time_sent = Carbon::now()->format('Y-m-d H:i:s');
        $message->received = false;
        $message->save();
        return -1;
    }

    public static function receiveMessages($sender_id, $receiver_id)
    {
    	Message::where('received', false)
    		->where('sender_id', $sender_id)
    		->where('receiver_id', $receiver_id)
    		->update(['received' => true]);
    }
}
