<?php

namespace App\Helpers;

use Auth;
use App\Models\User;
use App\Models\Role;

class Helper {
	public static function getError($error_code)
	{
		$errors = array(
			0 => 'Error.',
			100 => 'User ID does not exist.',
			101 => 'Username does not exist.',
			110 => 'Incorrect password.',
			111 => 'Invalid token.',
			112 => 'Expired token.',
			150 => 'Validation error.',
			151 => 'Registration failed.',

			200 => 'Requested friend does not exist.',
			210 => 'You are already friends.',
			211 => 'You already have a pending friend request with this user.',
			212 => 'No pending friend request from this user.',
			213 => 'This user is not a friend.',
			214 => 'Requested friend is blocked',
			215 => 'Requested friend blocked you.',
			252 => 'This user is already blocked.',
			253 => 'This user is not blocked.',

			300 => 'Messaged user does not exist.',
			310 => 'Message is empty.',

			400 => 'Requested user does not exist.',
		);

		return isset($errors[$error_code]) ? $errors[$error_code] : $errors[0];
	}

	public static function autoversion($file)
	{
		return $file.'?'.filemtime(public_path().$file);
	}

	public static function validateServerToken($token)
	{
		return array(
			'success' => true,
		);
	}

	public static function getAdminMainMenu($active = null) {
		$links = Helper::getAllAdminMenuItems();
		$user = Auth::user();

		if (is_null($user))
		{
			foreach ($links as $key => $item)
			{
				if (!empty($item->roles))
				{
					unset($links[$key]);
				}
			}
		}
		else
		{
			foreach ($links as $key => $item)
			{
				if (!empty($item->roles) && !in_array($user->role->id, $item->roles))
				{
					unset($links[$key]);
				}
			}
		}

		foreach ($links as $item)
		{
			if ($active == $item->url)
			{
				$item->active = true;
			}
			else
			{
				$item->active = false;
			}
		}

		return $links;
	}

	public static function getAllAdminMenuItems()
	{
		$links = array();

		$link = (object) [
			'url' => route('dashboard', array(), false),
			'title' => 'Dashboard',
			'roles' => array(Role::SUPERADMIN_ID,Role::ADMIN_ID),
			'submenu' => array(),
		];
		$links[] = $link;

		$users = array();

		$link = (object) [
			'url' => route('role-list', array(), false),
			'title' => 'Roles',
			'roles' => array(Role::SUPERADMIN_ID),
			'submenu' => array(),
		];
		$users[] = $link;

		$link = (object) [
			'url' => route('user-list', array(), false),
			'title' => 'Users',
			'roles' => array(Role::SUPERADMIN_ID),
			'submenu' => $users,
		];
		$links[] = $link;

		$server = array();

		$link = (object) [
			'url' => route('server-activity-monitor', array(), false),
			'title' => 'Activity Monitor',
			'roles' => array(Role::SUPERADMIN_ID),
			'submenu' => array(),
		];
		$server[] = $link;

		$link = (object) [
			'url' => route('server-ip-block', array(), false),
			'title' => 'IP Block',
			'roles' => array(Role::SUPERADMIN_ID),
			'submenu' => array(),
		];
		$server[] = $link;

		$link = (object) [
			'url' => route('server-activity-monitor', array(), false),
			'title' => 'Game Server',
			'roles' => array(Role::SUPERADMIN_ID),
			'submenu' => $server,
		];
		$links[] = $link;

		return $links;
	}

	public static function getPaginationOptions()
	{
		return array(
			'All',
			25,
			50,
			100,
			500,
		);
	}
}