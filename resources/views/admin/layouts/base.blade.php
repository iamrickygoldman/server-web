<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('admin.modules.head-content')

    @include('admin.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('admin.modules.header')

	@yield('toolbar')

	<div id="content" class="page-@yield('page-class')">	
	    <div class="container">

	    	@if (session('status-success'))
			    <div class="alert alert-success">
			        {{ session('status-success') }}
			    </div>
			@endif
			@if (session('status-warning'))
			    <div class="alert alert-warning">
			        {{ session('status-warning') }}
			    </div>
			@endif
			@if (session('status-error'))
			    <div class="alert alert-error">
			        {{ session('status-error') }}
			    </div>
			@endif

	    	@yield('content')
	    </div>
	</div>

    @include('admin.modules.footer')
    @include('admin.modules.scripts')
    @yield('inline-scripts')
</body>
</html>