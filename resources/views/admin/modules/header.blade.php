<header>
	<div class="logo-bar">
		<div class="container">
			<img class="logo normal-image" src="/images/logo.png" alt="{{env('APP_NAME', '')}}">
			<img class="logo retina-image" src="/images/logo@2x.png" alt="{{env('APP_NAME', '')}}">

			<nav class="user-nav">
			@guest
            	<a href="{{ route('login') }}">Login</a>
            	<a href="{{ route('register') }}">Register</a>
        	@endguest
        	@auth
				@if (Auth::user()->isAdmin())
				<a href="{{route('dashboard')}}">Admin</a>
				@endif

	            <a href="{{ route('logout') }}"
	                onclick="event.preventDefault();
	                         document.getElementById('logout-form').submit();">
	                Logout
	            </a>

	            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                {{ csrf_field() }}
	            </form>

	            <a href="{{ route('user-edit', ['id' => Auth::user()->id]) }}">{{ Auth::user()->name }}</a>
			@endauth
			</nav>

			<div class="toggle-slide-menu">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</div>
		</div>
	</div>
	{{-- edit menu items in Helper --}}
	<nav class="main-nav">
		<div class="container">

	@foreach ($mainMenu as $link)
		<span class="nav-item">
		@if ($link->active)
			<a href="{{$link->url}}" class="menu-item active">{{$link->title}}</a>
		@else
			<a href="{{$link->url}}" class="menu-item">{{$link->title}}</a>
		@endif
		@if (!empty($link->submenu))
			<div class="submenu">
			@foreach ($link->submenu as $sublink)
				<a href="{{$sublink->url}}" class="submenu-item">{{$sublink->title}}</a>
			@endforeach
			</div>
		@endif
		</span>
	@endforeach

		<div class="user-links">
		@guest
        	<a href="{{ route('login') }}">Login</a>
        	<a href="{{ route('register') }}">Register</a>
    	@endguest
    	@auth
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <a href="{{ route('user-edit', ['id' => Auth::user()->id]) }}">{{ Auth::user()->name }}</a>
		@endauth
		</div>

		</div>
	</nav>
</header>
<div class="slide-menu-overlay"></div>