@extends('admin.layouts.base')

@section('title', 'Edit Role')
@section('page-class', 'role')

@section('toolbar')

<div class="toolbar">
    <div class="container">
        <a href="{{ route('role-list') }}" class="btn btn-secondary">Back</a>
        <a href="javascript:void(0);" class="btn btn-primary large" onclick="document.roleForm.action = '{{ route('role-save-and-close') }}';document.roleForm.submit();">Save &amp; Close</a>
        <a href="javascript:void(0);" class="btn btn-primary" onclick="document.roleForm.submit();">Save</a>
    </div>
</div>

@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-error">
    @foreach ($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    </div>
@endif

<h1>Edit Role</h1>

<form method="POST" action="{{ route('role-save') }}" id="role-form" name="roleForm">
@csrf
<input id="id" type="hidden" name="id" value="{{ old('id') ? old('id') : $role->id }}">
<div class="input-row row">

    <div class="input-column col-sm-12">
        <label class="label-stn" for="title">Title</label>
        <input id="title" type="text" class="input-stn{{ $errors->has('title') ? ' error' : '' }}" name="title" value="{{ old('title') ? old('title') : $role->title }}" required autofocus>

        @if ($errors->has('title'))
        <span class="error-message">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-12">
        <label class="label-stn" for="description">Description</label>
        <textarea id="description" class="input-stn{{ $errors->has('description') ? ' error' : '' }}" name="description" required>{{ old('description') ? old('description') : $role->description }}</textarea>

        @if ($errors->has('description'))
            <span class="error-message">{{ $errors->first('description') }}</span>
        @endif
    </div>

</div>
</form>

@endsection