@extends('admin.layouts.base')

@section('title', 'List of Roles')
@section('page-class', 'roles')

@section('toolbar')

<div class="toolbar">
	<div class="container">
		<a href="{{ route('role-new') }}" class="btn btn-tertiary">New</a>
	</div>
</div>

@endsection

@section('content')

<h1>Roles</h1>

<div class="table-wrap">
<table class="table-stn table-scroll">
<thead>
	<tr>
		<th></th>
		<th>ID</th>
		<th>Name</th>
		<th>Description</th>
	</tr>
</thead>
<tbody>
@foreach($roles as $role)
	<tr>
	@if(in_array($role->id, array(1,2,3)))
		<td></td>
	@else
		<td class="links">
			<div class="links-wrap">
				<a title="edit" class="btn btn-tertiary btn-edit" href="{{ route('role-edit', ['id' => $role->id]) }}"></a>
				<form method="POST" action="{{ route('role-delete', ['id' => $role->id]) }}">
					@csrf
					<button title="delete" class="btn btn-danger btn-delete" type="submit" onclick="return confirm('Are you sure you want to permanently delete this role?');"></button>
				</form>
			</div>
		</td>
	@endif
		<td>{{$role->id}}</td>
		<td>{{$role->title}}</td>
		<td>{{$role->description}}</td>
	</tr>
@endforeach
</tbody>
</table>
</div>

@endsection