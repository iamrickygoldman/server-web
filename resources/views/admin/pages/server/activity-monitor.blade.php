@extends('admin.layouts.base')

@section('title', 'Server Activity Monitor')
@section('page-class', 'server-activity-monitor')

@section('toolbar')

<div class="toolbar">
	<div class="container">
	    <a class="btn btn-danger server-shutdown">Shutdown Server</a>
	</div>
</div>

@endsection

@section('content')

<form method="POST" action="/" style="display: none">
	@csrf
</form>

<h1>Server Activity Monitor</h1>
<h2>Active Connections: <span class="active-connections-count">{{count($current_connections)}}</span></h2>

<div class="table-wrap">
<table class="table-stn table-scroll">
<thead>
	<tr>
		<th></th>
		<th>IP Address</th>
		<th>Port</th>
		<th>User ID</th>
		<th>Time Connected</th>
	</tr>
</thead>
<tbody id="activity-content">
@foreach($current_connections as $connection)
	<tr>
		<td class="links">
			<div class="links-wrap">
				<a href="javascript:void(0);" title="disconnect" class="btn btn-danger btn-deactivate server-disconnect" data-ip="{{$connection->ip}}" data-port="{{$connection->port}}"></a>
			</div>
		</td>
		<td>{{$connection->ip}}</td>
		<td>{{$connection->port}}</td>
		<td>{{$connection->user_id}}</td>
		<td>{{$connection->time_connected}}</td>
	</tr>
@endforeach
</tbody>
</table>
</div>

@endsection

@section('inline-scripts')

<script type="text/javascript">
jQuery(function($){
	$(document).ready(function(){
		setInterval(function(){
			$.ajax({
				url: '/admin/server/activity-monitor-update',
				type: 'POST',
				data: {
					'_token': $('input[name="_token"').eq(0).val(),
				},
				success: function(data){
					console.log(data);
					$('#activity-content').empty();
					var html = '';
					for (var i = 0; i < data.current_connections.length; i++)
					{
						html += '<tr>';
						html += '<td class="links">';
						html += '<div class="links-wrap">';
						html += '<a href="javascript:void(0);" title="disconnect" class="btn btn-danger btn-deactivate server-disconnect" data-ip="'+data.current_connections[i].ip+'" data-port="'+data.current_connections[i].port+'"></a>';
						html += '</div>';
						html += '</td>';
						html += '<td>'+data.current_connections[i].ip+'</td>';
						html += '<td>'+data.current_connections[i].port+'</td>';
						var user_id = data.current_connections[i].user_id === null ? '' : data.current_connections[i].user_id;
						html += '<td>'+user_id+'</td>';
						html += '<td>'+data.current_connections[i].time_connected+'</td>';
						html += '</tr>';
					}
					$('#activity-content').html(html);
					$('.active-connections-count').text(data.current_connections.length);
					// Disconnect Connection from Server
					$('.server-disconnect').on('click',function(){
						if (confirm('Are you sure you wish to disconnect this active connection from the server?'))
						{
							$.ajax({
								url: '/admin/server/disconnect',
								type: 'POST',
								data: {
									'_token': $('input[name="_token"').eq(0).val(),
									'ip': $(this).data('ip'),
									'port': $(this).data('port'),
								},
								success: function(data){
									console.log(data);
								},
								error: function(error){
									console.log(error);
								},
							});
						}
					});
				},
				error: function(error){
					console.log(error);
				},
			});
		}, 5000);
	});
})
</script>

@endsection