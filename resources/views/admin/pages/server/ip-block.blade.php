@extends('admin.layouts.base')

@section('title', 'Server IP Block')
@section('page-class', 'server-ip-block')

@section('toolbar')

<div class="toolbar">
	<div class="container">
		<a href="javascript:void(0);" class="btn btn-primary" onclick="document.ipForm.submit();">Save</a>
	</div>
</div>

@endsection

@section('content')

<form method="POST" action="/" style="display: none">
	@csrf
</form>

<h1>IP Block Lists</h1>

<form action="{{ route('server-ip-block-save') }}" method="POST" name="ipForm" class="form-stn">
@csrf

<div class="input-row row">

    <div class="input-column col-sm-12">
        <label class="label-stn" for="ipv4">Ipv4</label>
		<textarea class="input-stn" name="ipv4">{{implode(PHP_EOL, $ipv4)}}</textarea>

		@if ($errors->has('title'))
	        <span class="error-message">{{ $errors->first('ipv4') }}</span>
	    @endif
	</div>

	<div class="input-column col-sm-12">
        <label class="label-stn" for="ipv6">Ipv6</label>
		<textarea class="input-stn" name="ipv6">{{implode(PHP_EOL, $ipv6)}}</textarea>

		@if ($errors->has('title'))
	        <span class="error-message">{{ $errors->first('ipv6') }}</span>
	    @endif
	</div>

</div>

@endsection