@extends('admin.layouts.base')

@section('title', 'Dashboard')
@section('page-class', 'dashboard')

@section('content')
	<h1>Dashboard</h1>

	<ul class="dashboard-tiles row">
	@foreach ($tiles as $tile)
		{!! $tile !!}
	@endforeach
	</ul>

@endsection