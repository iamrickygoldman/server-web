@extends('admin.layouts.base')

@section('title', 'Edit User')
@section('page-class', 'user')

@section('toolbar')

<div class="toolbar">
    <div class="container">
        <a href="{{ $backLink }}" class="btn btn-secondary">Back</a>
        <a href="javascript:void(0);" class="btn btn-primary large" onclick="document.userForm.action = '{{ route('user-save-and-close') }}';document.userForm.submit();">Save &amp; Close</a>
        <a href="javascript:void(0);" class="btn btn-primary" onclick="document.userForm.submit();">Save</a>
    </div>
</div>

@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-error">
    @foreach ($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    </div>
@endif

<h1>Edit User</h1>

<form method="POST" action="{{ route('user-save') }}" id="user-form" name="userForm">
@csrf
<input id="id" type="hidden" name="id" value="{{ old('id') ? old('id') : $user->id }}">
<div class="input-row row">

    <div class="input-column col-sm-6">
        <label class="label-stn" for="username">Username</label>
        <input id="username" type="text" class="input-stn{{ $errors->has('username') ? ' error' : '' }}" name="username" value="{{ old('username') ? old('username') : $user->username }}" required autofocus autocomplete="new-username">

        @if ($errors->has('username'))
        <span class="error-message">{{ $errors->first('username') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-6">
        <label class="label-stn" for="email">Email</label>
        <input id="email" type="email" class="input-stn{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') ? old('email') : $user->email }}" required autocomplete="new-email">

        @if ($errors->has('email'))
            <span class="error-message">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-6">
        <label class="label-stn" for="password">Password</label>
        <input id="password" type="password" class="input-stn{{ $errors->has('password') ? ' error' : '' }}" name="password" autocomplete="new-password">

        @if ($errors->has('password'))
        <span class="error-message">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-6">
        <label class="label-stn" for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" class="input-stn" name="password_confirmation">
    </div>

    @if (count($roles) > 0)
    <div class="input-column col-sm-6">
        <label class="label-stn" for="role_id">Role</label>
        <select id="role_id" class="select-stn sumo{{ $errors->has('role_id') ? ' error' : '' }}" name="role_id" placeholder="Select Role">
            <option value="">Select</option>
        @foreach ($roles as $role)
            <option value="{{ $role->id }}"{{ $user->role->id == $role->id ? ' selected' : '' }}>{{ $role->title }}</option>
        @endforeach
        </select>
    </div>
    @endif

</div>
</form>

@endsection