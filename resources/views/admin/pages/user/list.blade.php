@extends('admin.layouts.base')

@section('title', 'List of Users')
@section('page-class', 'users')

@section('toolbar')

<div class="toolbar">
	<div class="container">
		<form method="get" id="users-search" action="{{route('user-list')}}">
			<input type="hidden" id="filter_sort" name="filters[sort]" value="{{$filter_values['sort']}}">
			<input type="hidden" id="filter_direction" name="filters[direction]" value="{{$filter_values['direction']}}">
			<input type="text" name="filters[username]" class="input-stn" value="{{$filter_values['username']}}" placeholder="Filter Username">
			<input type="text" name="filters[email]" class="input-stn" value="{{$filter_values['email']}}" placeholder="Filter Email">
			<select name="filters[role_id]" class="select-stn sumo" placeholder="Filter Role">
	            <option value="">Filter Role</option>
	        @foreach ($roles as $role)
	            <option value="{{ $role->id }}"{{ $filter_values['role_id'] == $role->id ? ' selected' : '' }}>{{ $role->title }}</option>
	        @endforeach
	    	</select>
	    	<select name="filters[limit]" class="select-stn sumo" placeholder="List Limit">
	        @foreach ($pagination_options as $item)
	            <option value="{{ $item }}"{{ $filter_values['limit'] == $item ? ' selected' : '' }}>{{ $item }}</option>
	        @endforeach
	    	</select>

	        <input type="submit" class="btn btn-primary" value="Filter">
	        <a class="btn btn-secondary" href="{{route('user-list')}}">Reset</a>
	        </select>
		</form>
	</div>
</div>

@endsection

@section('content')

<h1>Users</h1>

<div class="table-wrap">
<table class="table-stn table-scroll">
<thead>
	<tr class="sortables">
		<th></th>
		<th class="sortable{{$filter_values['sort'] == 'id' ? ' filter filter-'.$filter_values['direction'] : ''}}" data-sort="id">ID</th>
		<th class="sortable{{$filter_values['sort'] == 'username' ? ' filter filter-'.$filter_values['direction'] : ''}}" data-sort="username">Username</th>
		<th class="sortable{{$filter_values['sort'] == 'role_id' ? ' filter filter-'.$filter_values['direction'] : ''}}" data-sort="role_id">Role</th>
		<th class="sortable{{$filter_values['sort'] == 'email' ? ' filter filter-'.$filter_values['direction'] : ''}}" data-sort="email">Email</th>
		<th class="sortable{{$filter_values['sort'] == 'mmr' ? ' filter filter-'.$filter_values['direction'] : ''}}" data-sort="mmr">MMR</th>
	</tr>
</thead>
<tbody>
@foreach($users as $user)
	<tr>
		<td class="links">
			<div class="links-wrap">
				<a title="edit" class="btn btn-tertiary btn-edit" href="{{ route('user-edit', ['id' => $user->id]) }}"></a>
			@if($user->trashed())
				<form method="POST" action="{{ route('user-activate', ['id' => $user->id]) }}">
					@csrf
					<button title="activate" class="btn btn-success btn-activate" type="submit"></button>
				</form>
				<form method="POST" action="{{ route('user-delete', ['id' => $user->id]) }}">
					@csrf
					<button title="delete" class="btn btn-danger btn-delete" type="submit" onclick="return confirm('Are you sure you want to permanently delete this family?');"></button>
				</form>
			@else
				<form method="POST" action="{{ route('user-deactivate', ['id' => $user->id]) }}">
					@csrf
					<button title="deactivate" class="btn btn-danger btn-deactivate" type="submit"></button>
				</form>
			@endif
			</div>
		</td>
		<td>{{$user->id}}</td>
		<td>{{$user->username}}</td>
		<td>{{$user->role->title}}</td>
		<td>{{$user->email}}</td>
		<td>{{$user->mmr}}</td>
	</tr>
@endforeach
</tbody>
</table>
</div>
{{ $users->links() }}

@endsection