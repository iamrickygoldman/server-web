<header>
	<div class="logo-bar">
		<div class="container">
			<img class="logo normal-image" src="/images/logo.png" alt="{{env('APP_NAME', '')}}">
			<img class="logo retina-image" src="/images/logo@2x.png" alt="{{env('APP_NAME', '')}}">

			<nav class="user-nav">
			@guest
            	<a href="{{ route('login') }}">Login</a>
            	<a href="{{ route('register') }}">Register</a>
        	@endguest
        	@auth
        		@if (Auth::user()->isAdmin())
				<a href="{{route('dashboard')}}">Admin</a>
				@endif
				
	            <a href="{{ route('logout') }}"
	                onclick="event.preventDefault();
	                         document.getElementById('logout-form').submit();">
	                Logout
	            </a>

	            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                {{ csrf_field() }}
	            </form>

	            <a href="{{ route('user-edit', ['id' => Auth::user()->id]) }}">{{ Auth::user()->name }}</a>
			@endauth
			</nav>
		</div>
	</div>
</header>