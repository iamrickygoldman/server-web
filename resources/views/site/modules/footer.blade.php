<footer>
	<div class="container">
		<p>Copyright &copy; {{date('Y')}} {{env('AUTHOR_NAME', '')}}</p>
	</div>
</footer>