@extends('site.layouts.base')

@section('title', 'Register User')
@section('page-class', 'register')

@section('content')

@if ($errors->any())
    <div class="alert alert-error">
    @foreach ($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    </div>
@endif

<h1>Register New User</h1>

<form method="POST" action="{{ route('register') }}" id="register-form">
@csrf

<div class="input-row row">

    <div class="input-column col-sm-6">
        <label class="label-stn" for="username">Username</label>
        <input id="username" type="text" class="input-stn{{ $errors->has('username') ? ' error' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

        @if ($errors->has('username'))
        <span class="error-message">{{ $errors->first('username') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-6">
        <label class="label-stn" for="email">Email Address</label>
        <input id="email" type="email" class="input-stn{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="error-message">{{ $errors->first('email') }}</span>
        @endif
    </div>
    
    <div class="input-column col-sm-6">
        <label class="label-stn" for="password">Password</label>
        <input id="password" type="password" class="input-stn{{ $errors->has('password') ? ' error' : '' }}" name="password" required>

        @if ($errors->has('password'))
        <span class="error-message">{{ $errors->first('password') }}</span>
        @endif
    </div>
    
    <div class="input-column col-sm-6">
        <label class="label-stn" for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" class="input-stn" name="password_confirmation" required>
    </div>

    <div class="submit-column col-sm-12">
        <input type="submit" class="submit-stn btn btn-primary" value="Register">
    </div>

</div>
</form>

@endsection