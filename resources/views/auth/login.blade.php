@extends('site.layouts.base')

@section('title', 'Login')
@section('page-class', 'login')

@section('content')

@if ($errors->any())
    <div class="alert alert-error">
    @foreach ($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    </div>
@endif

<h1>Login</h1>

<form method="POST" action="{{ route('login') }}" id="login-form">
@csrf

<div class="input-row row">

    <div class="input-column col-sm-12">
        <label class="label-stn" for="email">Username</label>
        <input id="email" type="text" class="input-stn{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

        @if ($errors->has('email'))
        <span class="error-message">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-12">
        <label class="label-stn" for="password">Password</label>
        <input id="password" type="password" class="input-stn{{ $errors->has('password') ? ' error' : '' }}" name="password" required>

        @if ($errors->has('password'))
        <span class="error-message">
        {{ $errors->first('password') }}</span>
        @endif
    </div>
    
    <div class="input-column col-sm-12">

        <div class="checkbox-stn">
            <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <div class="checkbox"></div>
                <span>{{ __('Remember Me') }}</span>
            </label>
        </div>

    </div>

    <div class="submit-column col-sm-12">
        <input type="submit" class="submit-stn btn btn-primary" value="Login">
    </div>

    <div class="notes-column col-sm-12">
        <a class="link-stn" href="{{ route('password.request') }}">Forgot Your Password?</a>
    </div>

</div>
</form>

@endsection