@extends('site.layouts.base')

@section('title', 'Verify Email')
@section('page-class', 'password-email')

@section('content')

<h1>Verify Your Email</h1>

@if (session('resent'))
    <div class="alert alert-success" role="alert">
        {{ __('A fresh verification link has been sent to your email address.') }}
    </div>
@endif

{{ __('Before proceeding, please check your email for a verification link.') }}
{{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
</form>

@endsection