@extends('site.layouts.base')

@section('title', 'Reset Password')
@section('page-class', 'password-reset')

@section('content')

<h1>New Password</h1>

<form method="POST" action="{{ route('password.request') }}" id="password-reset-form">
@csrf
<input type="hidden" name="token" value="{{ $token }}">
<div class="input-row row">

    <div class="input-column col-sm-12">
        <label class="label-stn" for="email">Email Address</label>
        <input id="email" type="email" class="input-stn{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

        @if ($errors->has('email'))
        <span class="error-message">{{ $errors->first('email') }}</span>
        @endif
    </div>
    <div class="input-column col-sm-12">
        <label class="label-stn" for="password">Password</label>
        <input id="password" type="password" class="input-stn{{ $errors->has('password') ? ' error' : '' }}" name="password" required>

        @if ($errors->has('password'))
        <span class="error-message">{{ $errors->first('password') }}</span>
        @endif
    </div>

    <div class="input-column col-sm-12">
        <label class="label-stn" for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" class="input-stn" name="password_confirmation" required>
    </div>

    <div class="submit-column col-sm-12">
        <input type="submit" class="btn btn-primary" value="Reset Password">
    </div>

</div>
</form>

@endsection