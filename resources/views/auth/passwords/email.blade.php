@extends('site.layouts.base')

@section('title', 'Reset Password')
@section('page-class', 'password-email')

@section('content')

<h1>Reset Password</h1>

<form method="POST" action="{{ route('password.email') }}" id="password-email-form">
@csrf

<div class="input-row row">

    <div class="input-column col-sm-12">
        <label class="label-stn" for="email">Email Address</label>
        <input id="email" type="email" class="input-stn{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
        <span class="error-message">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <div class="submit-column col-sm-12">
        <input type="submit" class="submit-stn btn btn-primary" value="Send Password Reset Link">
    </div>

</div>
</form>

@endsection