jQuery(function($){
	$(document).ready(function(){

		// Dropdowns
		$('.sumo').SumoSelect({
			placeholder: 'Select',
			floatWidth: 0,
		});
		$('.sumo-search').SumoSelect({
			placeholder: 'Select',
			floatWidth: 0,
			search: true,
		});

		// Mobile Slide Menu
		$('.toggle-slide-menu').on('click', function(){
			$('body').toggleClass('expanded');
			$('.main-nav').toggleClass('expanded');
		});
		$('.slide-menu-overlay').on('click', function(){
			$('body').removeClass('expanded');
			$('.main-nav').removeClass('expanded');
		});

		// Tabs used to toggle content panels
		var tabDropdown = $('#tab-dropdown');
		$('.tabs li a').on('click', function(){
			$('.tabs li a').removeClass('active');
			$(this).addClass('active');
			tabDropdown.val($(this).data('tab'));
			tabDropdown[0].sumo.reload();
			$('.toggleable').hide();
			$('#'+$(this).data('tab')).show();
		});
		tabDropdown.on('change', function(){
			$('.tabs li a').removeClass('active');
			$('.tabs li a[data-tab="'+$(this).val()+'"]').addClass('active');
			$('.toggleable').hide();
			$('#'+$(this).val()).show();
		});
		$('.toggleable').hide();
		$('#'+$('.tabs li a.active').data('tab')).show();

		// Sortable results list
		if ($('.sortables').length > 0)
		{
			$('.sortable').on('click', function(){
				if ($('#filter_sort').val() == $(this).data('sort'))
				{
					if ($('#filter_direction').val() == 'asc')
					{
						$('#filter_direction').val('desc');
					}
					else
					{
						$('#filter_direction').val('asc');
					}
				}
				else
				{
					$('#filter_sort').val($(this).data('sort'));
					$('#filter_direction').val('desc');
				}
				$('#filter_sort').closest('form').submit();
			});
		}

		// Show/Hide Toggle
		$('.show-toggle').on('click', function(){
			var ele = $('#'+$(this).data('show'));
			if ($(this).hasClass('collapsed'))
			{
				$(this).removeClass('collapsed');
				ele.slideDown();
			}
			else
			{
				$(this).addClass('collapsed');
				ele.slideUp();
			}
		});

		// Shutdown Server
		$('.server-shutdown').on('click',function(){
			if (confirm('Are you sure you wish to shut down the active game server?'))
			{
				$.ajax({
					url: '/admin/server/shutdown',
					type: 'POST',
					data: {
						'_token': $('input[name="_token"').eq(0).val(),
					},
					success: function(data){
						console.log(data);
					},
					error: function(error){
						console.log(error);
					},
				});
			}
		});

		// Disconnect Connection from Server
		$('.server-disconnect').on('click',function(){
			if (confirm('Are you sure you wish to disconnect this active connection from the server?'))
			{
				$.ajax({
					url: '/admin/server/disconnect',
					type: 'POST',
					data: {
						'_token': $('input[name="_token"').eq(0).val(),
						'ip': $(this).data('ip'),
						'port': $(this).data('port'),
					},
					success: function(data){
						console.log(data);
					},
					error: function(error){
						console.log(error);
					},
				});
			}
		});
	});
});