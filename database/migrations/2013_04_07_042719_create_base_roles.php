<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert(array(
            array(
                'title' => 'SuperAdmin',
                'description' => 'Unrestricted site access.',
            ),
            array(
                'title' => 'Admin',
                'description' => 'Limited admin site access.',
            ),
            array(
                'title' => 'User',
                'description' => 'Front end access only. Can play game.',
            ),
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')
            ->where('title', '=', 'SuperAdmin')
            ->orWhere('title', '=', 'Admin')
            ->orWhere('title', '=', 'User')
            ->delete();
    }
}
