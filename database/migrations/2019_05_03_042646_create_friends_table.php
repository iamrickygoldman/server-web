<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->integer('requester_id')->unsigned();
            $table->integer('receiver_id')->unsigned();
            $table->boolean('confirmed')->default(false);

            $table->foreign('requester_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('restrict');

            $table->primary(['requester_id', 'receiver_id']);
        });

        Schema::create('enemies', function (Blueprint $table) {
            $table->integer('requester_id')->unsigned();
            $table->integer('blocked_id')->unsigned();

            $table->foreign('requester_id')->references('id')->on('users')->onDelete('restrict');
            $table->foreign('blocked_id')->references('id')->on('users')->onDelete('restrict');

            $table->primary(['requester_id', 'blocked_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
        Schema::dropIfExists('enemies');
    }
}
