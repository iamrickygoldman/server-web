<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Game Server Address
    |--------------------------------------------------------------------------
    |
    | Specify the IP Address of the game server.
    |
    */

    'address' => env('GAME_SERVER_ADDRESS', '127.0.0.1'),

    /*
    |--------------------------------------------------------------------------
    | Game Server Port
    |--------------------------------------------------------------------------
    |
    | Specify the Port of the game server.
    |
    */

    'port' => env('GAME_SERVER_PORT', '6373'),

    /*
    |--------------------------------------------------------------------------
    | Game Server Shutdown Password
    |--------------------------------------------------------------------------
    |
    | Specify the shutdown password of the game server.
    |
    */

    'shutdown_password' => env('GAME_SERVER_SHUTDOWN_PASSWORD', 'secret'),

];
